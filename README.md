# Gousto coding test

Gousto coding test made by Christian Carestia.

Main libraries used:

- React JS / Create-react-app (as provided and for simplicity, could have used Webpack or ParcelJS)
- Redux for state management (no middleware used in this case)
- React-router-dom for routing
- Semantic UI as UI & icons library
- Utility libraries: fast.js, uuid
- Testing: Jest/Enzyme as provided in create-react-app (unit testing) + Cypress.io for integration/E2E testing

## How to use:

### Prerequisites:

- `node js` >= 8.12
- `yarn` (you can install it globally by running `npm install -g yarn`)

### First usage

- `git clone https://gitlab.com/chriscarex/gousto-react-test-cc.git` - clone the repository

### Commands:

- `yarn install` - installing dependencies
- `yarn start` - starting the app in dev mode
- `yarn start-windows` - starting the app in dev mode on windows
- `yarn build` - build the production artifacts
- `yarn test` - running unit tests
- `yarn cy:open` - running the integration/e2e tests

## Application structure

This is the main structure of the application:

- `public` - contains the index.html entry file
- `cypress` - contains Cypress integration tests
- `src` - contains the main source
  - `components` - contains the main components; each component contains:
    - `ComponentName.js` - stateless component - so that any other UI library can be applied while maintaining the same logic in case of changes;
    - `ComponentNameComponent.js` - stateful component containing the main logic and which interacts with redux and router ;
    - `styles.js` - JSS style;
    - `index.js` - export file;
  - `scenes` - are layout components which are specifically related to specific routes and usually contain one or more sub-components;
    - `SceneName.js` - stateless component - so that any other UI library can be applied while maintaining the same logic in case of changes;
    - `SceneNameComponent.js` - stateful component containing the main logic and which interacts with redux and router ;
    - `styles.js` - JSS style;
    - `index.js` - export file;
- `config` - contains config variables such as API endpoints
- `styles` - contains style variables which can be reused in multiple styles.js files (such as colors, grids, etc...)
- `utils` - contains utility functions
- `redux` - contains actions and reducers for state management
  - `actions` - contains redux actions
  - `reducers` - contains redux reducers
  - `constants.js` - contains redux-related constants
  - `store.js` - contains the redux store

## A list of missing functional requirements

- Related to task 1 and 2, having received CORS errors when making the GET requests, I mocked the response I obtained from Postman and used that instead from simplicity I would have solved it by using a NodeJS server with the proper headers acting as middleware as CORS are browser-related security issues.

All other requirements should be satisfied correctly

## Possible improvements/ functionality

Use webpack or ParcelJS instead of create-react-app.
Removing create-react-app would allow also the customisation of Eslint which I could not manage to configure using create-react-app
A package vulnerability scanner such as Snyk would be useful.
Addition unit tests for actions.
Definitely some styling - could have used css or scss but I kinda like JSS!
