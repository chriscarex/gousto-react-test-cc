/* eslint spaced-comment: 0 */
/// <reference types="Cypress" />

context("Task 2: As a user I want to see a list of products titles", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.viewport(1200, 700);
  });
  Cypress.on("uncaught:exception", () => false);

  it("Given that I am a user, When I land on the main page, Then I can see a list of products titles", () => {
    cy.get("[data-cy=product-item]")
      .its("length")
      .should("gt", 1);
  });
});
