/* eslint spaced-comment: 0 */
/// <reference types="Cypress" />

context(
  "Task 4: As a user I want to be able to search in the product title and description",
  () => {
    beforeEach(() => {
      cy.visit("/");
      cy.viewport(1200, 700);
    });
    Cypress.on("uncaught:exception", () => false);

    it("Given that I am a user, Given that I am a user and I land on the 'Drinks Cabinet' When I type ' serv' in the input search, Then the products matching the search input in title and/or description are shown below", () => {
      cy.get("[data-cy=category-item]:first").click();
      cy.get("[data-cy=search] input").type(" serv");
      cy.get("[data-cy=product-item]")
        .its("length")
        .should("eq", 1);
    });
  }
);
