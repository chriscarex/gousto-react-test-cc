/* eslint spaced-comment: 0 */
/// <reference types="Cypress" />

context(
  "Task 6: As a user I want to be able to navigate with the browser's native back and forward buttons",
  () => {
    beforeEach(() => {
      cy.visit("/category/drinks-cabinet");
      cy.viewport(1200, 700);
    });
    Cypress.on("uncaught:exception", () => false);

    it("Given that I am a user When I am on the 'Drinks Cabinet' category And I click on 'Large Alcohol' category Then I can click on the back button from the browser And I can see the 'Drinks Cabinet' category selected Then I click forward button And I can see the 'Large Alcohol' category selected", () => {
      cy.get("[data-cy=category-item]:nth-child(2)").click();
      cy.go(-1);
      cy.get("[data-cy=category-item]:first").should(
        "have.css",
        "font-weight",
        "700"
      );
      cy.go(1);
      cy.get("[data-cy=category-item]:nth-child(2)").should(
        "have.css",
        "font-weight",
        "700"
      );
    });
  }
);
