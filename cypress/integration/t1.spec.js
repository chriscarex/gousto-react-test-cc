/* eslint spaced-comment: 0 */
/// <reference types="Cypress" />

context(
  "Task 1: As a user I want to see all available product categories",
  () => {
    beforeEach(() => {
      cy.visit("/");
      cy.viewport(1200, 700);
    });
    Cypress.on("uncaught:exception", () => false);

    it("Given that I am a user, When I land on the main page, Then I can see the categories of products", () => {
      cy.get("[data-cy=header]");
      cy.get("[data-cy=category-item]")
        .its("length")
        .should("eq", 7);
    });
  }
);
