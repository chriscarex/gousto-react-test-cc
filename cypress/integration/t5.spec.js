/* eslint spaced-comment: 0 */
/// <reference types="Cypress" />

context(
  "Task 5: As a user I want to be able to see the product description when I click on the product name",
  () => {
    beforeEach(() => {
      cy.visit("/");
      cy.viewport(1200, 700);
    });
    Cypress.on("uncaught:exception", () => false);

    it("Given that I am a user and I land on main page When I click on 'Borsao Macabeo' Then I can see the description appearing below and the title is bold When I click again on the 'Borsao Macabeo' Then I can see that the description is hidden When I click on multiple products Then all the clicked products descriptions are visible", () => {
      cy.get("[data-cy=product-item]:first").click();
      cy.get("[data-cy=product-item-description]")
        .its("length")
        .should("eq", 1);
      cy.get("[data-cy=product-item]:first").click();
      cy.get("[data-cy=product-item-description]").should("not.exist");
      cy.get("[data-cy=product-item]:first").click();
      cy.get("[data-cy=product-item]:nth-child(2)").click();
      cy.get("[data-cy=product-item-description]")
        .its("length")
        .should("eq", 2);
    });
  }
);
