/* eslint spaced-comment: 0 */
/// <reference types="Cypress" />

context(
  "Task 3: As a user I want to see the products for the selected category",
  () => {
    beforeEach(() => {
      cy.visit("/");
      cy.viewport(1200, 700);
    });
    Cypress.on("uncaught:exception", () => false);

    it("Given that I am a user, When I land on the main page view, And I click on 'Drinks Cabinet' category, Then I can see a list of products belonging to that category, And the selected category is bold", () => {
      cy.get("[data-cy=category-item]:first").click();
      cy.get("[data-cy=product-item]")
        .its("length")
        .should("eq", 16);
      cy.get("[data-cy=category-item]:first").should(
        "have.css",
        "font-weight",
        "700"
      );
    });
  }
);
