import React from "react";
import PropTypes from "prop-types";
import CardItem from "../../components/CardItem";
import uuidv4 from "uuid/v4";
import { styles } from "./styles";

const Category = ({ products, category }) => {
  return (
    <div style={styles.container} data-cy="homepage">
      <div style={styles.internalContainer}>
        <div style={styles.title}>
          LIST OF PRODUCTS{category ? ` for ${category}` : ""}
        </div>
        <div style={styles.listContainer}>
          {products.length > 0 &&
            products.map(product => (
              <CardItem key={"product-" + uuidv4()} product={product} />
            ))}
          {products.length === 0 && (
            <p>I should have mocked these products as well! :)</p>
          )}
        </div>
      </div>
    </div>
  );
};

Category.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  category: PropTypes.string.isRequired
};

export default Category;
