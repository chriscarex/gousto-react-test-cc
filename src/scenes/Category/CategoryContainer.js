import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { filterByCategoryAndSearch, urlToTitle } from "../../utils";
import { setCategory } from "../../redux/actions";

import Category from "./Category";

class CategoryContainer extends Component {
  componentWillMount() {
    const { match, setCategoryAction } = this.props;
    setCategoryAction(urlToTitle(match.params.id));
  }

  componentDidUpdate(prevProps) {
    const { match, setCategoryAction } = this.props;

    if (match.params.id !== prevProps.match.params.id) {
      setCategoryAction(urlToTitle(match.params.id));
    }
  }

  filterProducts = () => {
    const { products, category, search } = this.props;

    return filterByCategoryAndSearch(products, category, search);
  };

  render() {
    const { category } = this.props;

    const filteredProducts = this.filterProducts();
    return <Category products={filteredProducts} category={category} />;
  }
}

CategoryContainer.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  category: PropTypes.string.isRequired,
  search: PropTypes.string.isRequired,
  setCategoryAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  products: state.products,
  category: state.categories.selectedCategory,
  search: state.search
});

const mapDispatchToProps = dispatch => {
  return {
    setCategoryAction: props => dispatch(setCategory(props))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CategoryContainer)
);
