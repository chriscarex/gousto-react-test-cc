import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { filterByCategoryAndSearch } from "../../utils";
import { setCategory } from "../../redux/actions";
import Homepage from "./Homepage";

class HomepageContainer extends Component {
  componentWillMount() {
    const { setCategoryAction } = this.props;
    setCategoryAction("");
  }

  filterProducts = () => {
    const { products, category, search } = this.props;

    return filterByCategoryAndSearch(products, category, search);
  };

  render() {
    const filteredProducts = this.filterProducts();
    return <Homepage products={filteredProducts} />;
  }
}

HomepageContainer.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  category: PropTypes.string.isRequired,
  search: PropTypes.string.isRequired,
  setCategoryAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  products: state.products,
  category: state.categories.selectedCategory,
  search: state.search
});

const mapDispatchToProps = dispatch => {
  return {
    setCategoryAction: props => dispatch(setCategory(props))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(HomepageContainer)
);
