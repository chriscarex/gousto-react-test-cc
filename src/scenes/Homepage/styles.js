import { FONT_COLOR } from "../../styles";

export const styles = {
  container: {
    paddingTop: 120,
    width: "100%"
  },
  internalContainer: {
    width: "100%",
    maxWidth: "1300px",
    margin: "auto"
  },
  title: {
    color: FONT_COLOR,
    fontSize: 24,
    fontWeight: 700,
    width: "100%",
    textAlign: "center",
    marginTop: 20
  },
  listContainer: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
    justifyContent: "center",
    marginTop: 20
  }
};
