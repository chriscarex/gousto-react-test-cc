import fast from "fast.js";

export const toCamelCase = word =>
  word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();

export const titleToUrl = title => title.toLowerCase().replace(" ", "-");

export const urlToTitle = title => {
  const titleArray = title.split("-");

  if (titleArray.length > 0) {
    return fast.map(titleArray, word => toCamelCase(word)).join(" ");
  }
  return toCamelCase(titleArray);
};
