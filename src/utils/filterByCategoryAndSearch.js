import fast from "fast.js";

export const filterBySearch = (product, search) => {
  if (
    search === "" ||
    product.title.toLowerCase().indexOf(search.toLowerCase()) > -1 ||
    product.description.toLowerCase().indexOf(search.toLowerCase()) > -1
  ) {
    return true;
  }

  return false;
};

export const filterByCategoryAndSearch = (products, category, search) => {
  const filteredProducts = [];

  if (category === "" && search === "") {
    return products;
  }

  products.filter(product => {
    if (product.categories && product.categories.length > 0) {
      let wasAdded = false;
      fast.map(product.categories, cat => {
        if (
          !wasAdded &&
          ((cat.title && cat.title === category) || category === "")
        ) {
          if (filterBySearch(product, search)) {
            wasAdded = true;
            filteredProducts.push(product);
          }
        }
        return null;
      });
    }
    return null;
  });

  return filteredProducts;
};
