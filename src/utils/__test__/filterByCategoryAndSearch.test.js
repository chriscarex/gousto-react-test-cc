import { PRODUCTS } from "../../config/fixtures";
import { filterByCategoryAndSearch, filterBySearch } from "..";
import expect from "expect";

describe("filterByCategoryAndSearch", () => {
  it("should return entire list", () => {
    expect(filterByCategoryAndSearch(PRODUCTS.data, "", "")).toEqual(
      PRODUCTS.data
    );
  });

  it("should return a limited list filtered by category", () => {
    expect(
      filterByCategoryAndSearch(PRODUCTS.data, "Drinks Cabinet", "")
    ).toHaveLength(16);
  });

  it("should return a limited list filtered by category and search", () => {
    expect(
      filterByCategoryAndSearch(PRODUCTS.data, "Drinks Cabinet", " serv")
    ).toHaveLength(1);
  });

  it("should return entire list", () => {
    expect(filterBySearch(PRODUCTS.data[0], "")).toEqual(true);
  });

  it("should return a limited list filtered by category and search", () => {
    expect(filterBySearch(PRODUCTS.data[0], " serv")).toEqual(false);
  });
});
