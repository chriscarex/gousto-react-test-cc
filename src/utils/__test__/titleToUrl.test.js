import { PRODUCTS } from "../../config/fixtures";
import { titleToUrl, urlToTitle, toCamelCase } from "..";
import expect from "expect";

describe("titleToUrl", () => {
  it("should turn title to url", () => {
    expect(titleToUrl("Test Case")).toEqual("test-case");
  });

  it("should turn url to title (single word)", () => {
    expect(urlToTitle("test")).toEqual("Test");
  });

  it("should turn url to title (multiple words)", () => {
    expect(urlToTitle("test-case")).toEqual("Test Case");
  });

  it("should turn word to CamelCase", () => {
    expect(toCamelCase("test")).toEqual("Test");
  });
});
