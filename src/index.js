import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import Startup from "./components/Startup";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import Homepage from "./scenes/Homepage";
import Category from "./scenes/Category";
import registerServiceWorker from "./registerServiceWorker";
import { store } from "./redux/store";
import "semantic-ui-css/semantic.min.css";
import { HOMEPAGE, CATEGORY } from "./config/routes";
import "./index.css";

window.store = store;

const App = (
  <Provider store={store}>
    <BrowserRouter basename="/">
      <Fragment>
        <Startup />
        <Header />
        <SearchBar />
        <Switch>
          <Route exact path={HOMEPAGE} component={Homepage} />
          <Route exact path={CATEGORY} component={Category} />
        </Switch>
      </Fragment>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(App, document.getElementById("root"));

registerServiceWorker();
