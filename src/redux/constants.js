export const GET_CATEGORIES = "GET_CATEGORIES";
export const GET_PRODUCTS = "GET_PRODUCTS";
export const SET_CATEGORY = "SET_CATEGORY";
export const SET_SEARCH = "SET_SEARCH";
