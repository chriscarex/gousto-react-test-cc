import fetch from "cross-fetch";
import { GET_CATEGORIES, SET_CATEGORY } from "../constants";
import { API_CATEGORIES } from "../../config/endpoints";

export const getCategories = () => dispatch =>
  fetch(API_CATEGORIES, {
    method: "GET",
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    credentials: "same-origin"
  })
    .then(res => {
      if (res.status >= 400) {
        dispatch({
          type: GET_CATEGORIES
        });
        return false;
      }
      return res.json();
    })
    .then(data => {
      dispatch({
        type: GET_CATEGORIES,
        data
      });
      return true;
    })
    .catch(err => {
      dispatch({
        type: GET_CATEGORIES
      });

      return false;
    });

export const setCategory = data => dispatch =>
  dispatch({
    type: SET_CATEGORY,
    data
  });
