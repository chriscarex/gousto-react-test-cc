export { getCategories, setCategory } from "./categories";
export { getProducts } from "./products";
export { setSearch } from "./search";
