import fetch from "cross-fetch";
import { GET_PRODUCTS } from "../constants";
import { API_PRODUCTS } from "../../config/endpoints";

export const getProducts = () => dispatch =>
  fetch(API_PRODUCTS, {
    method: "GET",
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    credentials: "same-origin"
  })
    .then(res => {
      if (res.status >= 400) {
        dispatch({
          type: GET_PRODUCTS
        });
        return false;
      }
      return res.json();
    })
    .then(data => {
      dispatch({
        type: GET_PRODUCTS,
        data
      });
      return true;
    })
    .catch(err => {
      dispatch({
        type: GET_PRODUCTS
      });

      return false;
    });
