import { SET_SEARCH } from "../constants";

export const setSearch = data => dispatch =>
  dispatch({
    type: SET_SEARCH,
    data
  });
