import { GET_CATEGORIES, SET_CATEGORY } from "../../constants";
import { CATEGORIES } from "../../../config/fixtures";
import { categories } from "../categories";
import expect from "expect";

describe("Categories reducer", () => {
  it("should return the initial state", () => {
    const returningState = { categories: [], selectedCategory: "" };
    expect(categories(undefined, {})).toEqual(returningState);
  });

  it("should handle GET_CATEGORIES", () => {
    const getCategories = {
      type: GET_CATEGORIES,
      data: null
    };
    const returningCategories = [
      CATEGORIES.data[0],
      ...CATEGORIES.data.slice(Math.max(CATEGORIES.data.length - 6, 1))
    ];

    expect(categories({}, getCategories).categories).toEqual(
      returningCategories
    );
  });

  it("should handle SET_CATEGORIES", () => {
    const returningValue = "test";
    const setCategory = {
      type: SET_CATEGORY,
      data: returningValue
    };

    expect(categories({}, setCategory).selectedCategory).toEqual(
      returningValue
    );
  });
});
