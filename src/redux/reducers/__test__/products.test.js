import { GET_PRODUCTS } from "../../constants";
import { PRODUCTS } from "../../../config/fixtures";
import { products } from "../products";
import expect from "expect";

describe("Products reducer", () => {
  it("should return the initial state", () => {
    expect(products(undefined, {})).toEqual([]);
  });

  it("should handle GET_PRODUCTS", () => {
    const getProducts = {
      type: GET_PRODUCTS,
      data: null
    };
    const returningList = PRODUCTS.data;

    expect(products({}, getProducts)).toEqual(returningList);
  });
});
