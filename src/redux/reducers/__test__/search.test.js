import { SET_SEARCH } from "../../constants";
import { search } from "../search";
import expect from "expect";

describe("Search reducer", () => {
  it("should handle SET_SEARCH", () => {
    const returningValue = "test";
    const setSearch = {
      type: SET_SEARCH,
      data: returningValue
    };

    expect(search({}, setSearch)).toEqual(returningValue);
  });
});
