import { combineReducers } from "redux";
import { categories } from "./categories";
import { products } from "./products";
import { search } from "./search";

const reducers = combineReducers({
  categories,
  products,
  search
});

export default reducers;
