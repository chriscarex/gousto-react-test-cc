import { GET_CATEGORIES, SET_CATEGORY } from "../constants";
import { CATEGORIES } from "../../config/fixtures";

const initialState = {
  categories: [],
  selectedCategory: ""
};

export function categories(state = initialState, action) {
  switch (action.type) {
    case GET_CATEGORIES:
      const fullList = action.data || CATEGORIES.data;
      const returningList = [
        fullList[0],
        ...fullList.slice(Math.max(fullList.length - 6, 1))
      ];
      return { ...state, categories: returningList };
    case SET_CATEGORY:
      const selectedCategory = action.data || "";

      return { ...state, selectedCategory };
    default:
      return state;
  }
}
