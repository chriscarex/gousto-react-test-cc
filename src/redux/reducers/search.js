import { SET_SEARCH } from "../constants";

const initialState = "";

export function search(state = initialState, action) {
  switch (action.type) {
    case SET_SEARCH:
      return action.data || "";
    default:
      return state;
  }
}
