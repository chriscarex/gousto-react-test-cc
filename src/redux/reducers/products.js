import { GET_PRODUCTS } from "../constants";
import { PRODUCTS } from "../../config/fixtures";

export function products(state = [], action) {
  switch (action.type) {
    case GET_PRODUCTS:
      return action.data || PRODUCTS.data;
    default:
      return state;
  }
}
