export const API_CATEGORIES =
  "https://api.gousto.co.uk/products/v2.0/categories";
export const API_PRODUCTS =
  "https://api.gousto.co.uk/products/v2.0/products?includes[]=categories&image_sizes[]=365";
