export const CATEGORIES = {
  status: "ok",
  meta: {
    offset: 0,
    limit: 1,
    count: 1,
    total: 1
  },
  data: [
    {
      id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
      title: "Drinks Cabinet",
      box_limit: 7,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "529ea59e-bf7e-11e5-840e-02fada0dd3b9",
      title: "Kitchenware",
      box_limit: 6,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "fec10d0e-bf7d-11e5-90a9-02fada0dd3b9",
      title: "Desserts",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
      title: "Food Cupboard",
      box_limit: 10,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
      title: "Snacks",
      box_limit: 10,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "c2b64b58-5280-11e6-b304-02cd0b1dc697",
      title: "Kids",
      box_limit: 5,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "287ad10e-e186-11e6-86d4-0297f19e8e45",
      title: "Valentines",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "384da59c-9452-11e6-87b0-06981aa90cf9",
      title: "Christmas",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "831b3f8a-3854-11e6-86cf-06f9522b85fb",
      title: "Small BakedIn",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "4cb35a02-27da-11e6-bf1e-026e15ef4a55",
      title: "Lunch",
      box_limit: 5,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "4dd32756-d12e-11e7-b28b-0617e74d8914",
      title: "Christmas md",
      box_limit: 3,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "a44b4c22-e489-11e6-9d77-027ca04bdf39",
      title: "Most Popular",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "89e489de-3854-11e6-ae7c-06f9522b85fb",
      title: "Large Breakfast",
      box_limit: 1,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "7867356c-3854-11e6-ab35-06f9522b85fb",
      title: "Mini Alcohol",
      box_limit: 5,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "dd25600c-97c9-11e7-b173-067ba2f0c390",
      title: "Free from",
      box_limit: 4,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "830f0a62-3854-11e6-8999-06f9522b85fb",
      title: "Large BakedIn",
      box_limit: 1,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "48dc8dc2-27da-11e6-9902-026e15ef4a55",
      title: "Breakfast",
      box_limit: 6,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "47d0e046-d12e-11e7-aba4-0617e74d8914",
      title: "Christmas sm",
      box_limit: 4,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "83281818-3854-11e6-8771-06f9522b85fb",
      title: "Mug BakedIn",
      box_limit: 4,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "785741fc-3854-11e6-87a5-06f9522b85fb",
      title: "Large Alcohol",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "52a565d2-d12e-11e7-a210-0617e74d8914",
      title: "Christmas lg",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "8b349cfc-ff3c-11e6-bf7e-02d1dd7f0ccb",
      title: "Easter",
      box_limit: 4,
      is_default: false,
      recently_added: false,
      hidden: false
    },
    {
      id: "89f0f70a-3854-11e6-8218-06f9522b85fb",
      title: "Small Breakfast",
      box_limit: 5,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "8302b6ae-3854-11e6-9e42-06f9522b85fb",
      title: "Little Pot",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: true
    },
    {
      id: "f76c6640-97c9-11e7-aecd-067ba2f0c390",
      title: "Small free from",
      box_limit: 2,
      is_default: false,
      recently_added: false,
      hidden: true
    }
  ]
};

export const PRODUCTS = {
  status: "ok",
  meta: {
    offset: 0,
    limit: 1,
    count: 1,
    total: 1
  },
  data: [
    {
      id: "0009468c-33e9-11e7-b485-02859a19531d",
      sku: "AP-ACH-WIN-WHI-23-P",
      title: "Borsao Macabeo",
      description:
        "A flavoursome Summer wine made from the indigenous Macabeo grape in northern Spain. A balanced, modern white with flavours of ripe peach, zesty lemon and nutty undertones, it leaves the palate with a clean and fruity finish.",
      list_price: "6.95",
      is_vatable: true,
      is_for_sale: true,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-05-08T13:22:27+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-05-08T13:22:46+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-05-08T13:22:46+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/YAddOns-WhiteWines-Borsao_013244-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/YAddOns-WhiteWines-Borsao_013244-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "00a0130e-bfea-11e7-a2c2-0617e74d8914",
      sku: "AP-FCD-BIS-06",
      title: "Love Shortie All Butter Shortbread",
      description:
        "A rich all butter shortbread, delicately sweet and crumbly with a hint of sea salt, straight out of Scotland. ",
      list_price: "3.95",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-11-02T16:22:21+00:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-12-29T14:55:56+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Shortbread-0663-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Shortbread-0663-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0126601a-26df-11e8-a736-0239a66a4b36",
      sku: "AP-ACH-WIN-ROS-04-P",
      title: "Domaine de L'Olibet 'Les Pujols' Cinsault Rosé",
      description:
        "A little gem from Saint Georges d’Orques in the south of France. Delicate, sweet spice aromas lead on to a palate of abundant ripe fruit, lifted by a natural zing and finishing with texture and richness. Full of character. 11.5% ABV [France]",
      list_price: "9.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: 0,
      zone: "Ambient",
      created_at: "2018-03-13T16:53:07+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2018-03-13T16:54:41+00:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2018-03-13T16:54:41+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Domaine-de-LOlibet-Rose_Market-Place0594-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Domaine-de-LOlibet-Rose_Market-Place0594-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "01e33b6a-0ce7-4065-b4b9-d59de0828240",
      sku: "G-063",
      title: "We Are Tea A6 Notes",
      description: "We Are Tea A6 Notes",
      list_price: "0.00",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: null,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-12-22T11:05:52+00:00",
      categories: [],
      tags: ["gift"],
      images: {
        "365": null
      }
    },
    {
      id: "02c225f2-63b4-11e6-8516-023d2759e21d",
      sku: "AP-ACH-WIN-RED-21-P",
      title: "Rioja Reserva, Baron de Ebro",
      description:
        "Intriguing notes of vanilla and chocolate make this fruity, full-bodied wine unique. With a long, pleasant finish and woody aroma supporting it, this characterful wine is a good match with beef dishes.\nABV 14%.",
      list_price: "9.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-08-16T13:19:07+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-31T12:32:39+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2016-08-31T12:32:39+01:00"
          }
        },
        {
          id: "a44b4c22-e489-11e6-9d77-027ca04bdf39",
          title: "Most Popular",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-02-08T09:49:46+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Wines-Red-BaronBayRiojaReserva_26683-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Wines-Red-BaronBayRiojaReserva_26683-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "03117030-9127-11e6-8019-02886f33f4e9",
      sku: "AP-ACH-WIN-WHI-16-P",
      title: "Bantry Bay Chenin Blanc",
      description:
        "South Africa have adopted the Chenin Blanc grape, and to great effect, showcased in this wine. With delicate floral notes and a subtle pineapple flavour joined by striking lemon and grapefruit notes, it pairs particularly well with seafood. \nABC 12.5%",
      list_price: "7.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 1,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-10-13T09:25:42+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-10-13T09:26:04+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2016-10-13T09:26:04+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/_White-BantryBay_01625-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/_White-BantryBay_01625-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "05de669c-33e8-11e7-94c4-02859a19531d",
      sku: "AP-ACH-WIN-RED-32-P",
      title: "Nieto Malbec, Mendoza",
      description:
        "This Argentinian Malbec is incredibly juicy, fresh and tasty! Forty-year-old vines yield concentrated blackberry and plum fruit flavours, with a touch of sweet spice. The palate is silky and warming, with more black fruit and cherries. A great wine with a fresh finish. 13.5% ABV [Argentina]",
      list_price: "12.45",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: 0,
      zone: "Ambient",
      created_at: "2017-05-08T13:15:28+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-05-08T13:15:53+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-05-08T13:15:53+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Nieto-Malbec_Market-Place0554-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Nieto-Malbec_Market-Place0554-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0667b7de-025c-11e7-9821-02d559812e87",
      sku: "AP-ACH-WIN-WHI-22-P",
      title: "Sancerre Blanc",
      description:
        "Per 100g serving, Energy: 604kj, Energy: 144 cal, Fat: 7.5g, of which saturates: 4.5g, Carbodhydrate 13g, of which; sugars: 1.9g, protein: 6.7g, salt 0.8g. Allergens: Milk, Wheat, Fish, Mustard",
      list_price: "14.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: 1400,
      zone: "Ambient",
      created_at: "2017-03-06T10:59:52+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-06T11:45:36+00:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-03-06T11:45:36+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-PremiumWines-Sancerre_05721-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-PremiumWines-Sancerre_05721-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "069f7f26-025b-11e7-81e2-0234acc3da5b",
      sku: "AP-FCD-SHB-20",
      title: "Choc on Choc Make Your Own Egg Kit",
      description:
        "It doesn't get more egg-cellent than this make your own chocolate egg kit. Allowing kids to mould and decorate their very own Easter egg with their favourite sweets. Arriving with a paper chef's hat, nothing will be more special than seeing your little ones turn into mini chocolatiers for the day!",
      list_price: "9.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-03-06T10:52:43+00:00",
      categories: [
        {
          id: "fec10d0e-bf7d-11e5-90a9-02fada0dd3b9",
          title: "Desserts",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-06T12:05:39+00:00"
          }
        },
        {
          id: "830f0a62-3854-11e6-8999-06f9522b85fb",
          title: "Large BakedIn",
          box_limit: 1,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-03-06T12:05:39+00:00"
          }
        },
        {
          id: "a44b4c22-e489-11e6-9d77-027ca04bdf39",
          title: "Most Popular",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-31T12:06:15+01:00"
          }
        },
        {
          id: "8b349cfc-ff3c-11e6-bf7e-02d1dd7f0ccb",
          title: "Easter",
          box_limit: 4,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-06T12:05:39+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-ChocEggHeads_05965-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-ChocEggHeads_05965-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "073079f4-0265-11e7-9ce9-06e91e000dc9",
      sku: "AP-FCD-CHO-21",
      title: "Green & Blacks Sea Salt Dark Chocolate Thins",
      description:
        "Infusing rich, dark chocolate with a subtle hint of Anglesey Sea Salt - nothing says 'treat yourself' like this new delight from Green & Black's. Delicately thin but deliciously crunchy, these squares of dark, organic chocolate are best broken and enjoyed alone. (Only share if you really must!)",
      list_price: "2.25",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-03-06T12:04:19+00:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-08T09:56:35+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-GBThinDarkChocWSeaSalt_05993-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-GBThinDarkChocWSeaSalt_05993-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "085e4fea-4feb-11e6-88dc-0630ecb1239f",
      sku: "AP-FCD-ICC-02",
      title: "Smooze Coconut And Mango Fruit Ice Multipack (GF) (DF)",
      description:
        "How Smooze have created fruit ices that are this full of flavour, but low calorie, dairy free, gluten free, nut free and without preservatives or sweeteners is beyond us, but we're glad they have! Shake then freeze at home. Each box contains 5 fruit ices.",
      list_price: "2.45",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-07-22T09:02:35+01:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-07-03T15:19:04+01:00"
          }
        },
        {
          id: "c2b64b58-5280-11e6-b304-02cd0b1dc697",
          title: "Kids",
          box_limit: 5,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-16T16:49:52+01:00"
          }
        },
        {
          id: "dd25600c-97c9-11e7-b173-067ba2f0c390",
          title: "Free from",
          box_limit: 4,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-09-19T10:57:36+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-MangoSmooze_23378-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-MangoSmooze_23378-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0908dcd6-6bf3-11e9-874a-06b8806e87f2",
      sku: "AP-FCD-CSN-40",
      title: "Espresso Martini Popcorn Joe & Seph's (70g)",
      description:
        "Joe & Seph's smooth Caramel infused with Coffee Liqueur and Vodka, followed by a wicked shot of Espresso. Made with 5% real spirits.\n\nIngredients:\nCorn, sugar, butter (milk), corn syrup, double cream (milk), spirits 5% (coffee liqueur, vodka), coffee.\n\nMade in an environment where nuts, peanuts and sesame are present.",
      list_price: "2.99",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: 1200,
      zone: "Ambient",
      created_at: "2019-05-01T10:25:18+01:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2019-05-01T10:27:00+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Espresso-Martini-Joe--Sephs-Popcorn.2-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Espresso-Martini-Joe--Sephs-Popcorn.2-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "09cd7fa2-16b3-11e6-aefa-06de43ff1e17",
      sku: "AP-FCD-CSN-12",
      title: "Serious Pig Chilli & Paprika Snacking Salami",
      description:
        "Using the finest British pork, these scrumptious salami sticks carefully balance the sharp piquancy of chilli and the smokiness of paprika to create a deep, rich flavour which won them a Great Taste Award!",
      list_price: "1.50",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 10,
      always_on_menu: false,
      volume: 0,
      zone: "Ambient",
      created_at: "2016-05-10T13:28:10+01:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-18T14:10:59+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Serious-Pig_Snacking-Salami_chilli--paprika-1562_RESIZED-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Serious-Pig_Snacking-Salami_chilli--paprika-1562_RESIZED-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0a8e892c-1780-11e6-9f77-0255e2c77e1d",
      sku: "AP-FCD-CON-07",
      title: "Balsajo Black Garlic Cloves",
      description:
        "What happens if you leave a premium garlic bulb in an oven for a few weeks to slow cook? You get this wonderful stuff! With a sweet, strong and balsamicky taste, it’s a seriously exciting ingredient to try out in your cooking.",
      list_price: "2.95",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 5,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-05-11T13:55:38+01:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-16T11:57:59+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Balsajo-BlackGarlic_17876-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Balsajo-BlackGarlic_17876-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0a947ece-c6ab-11e6-8a16-02eaa658c421",
      sku: "AP-FCD-CER-07",
      title: "Superbake Morning Dreamer Pancakes (GF) (DF)",
      description:
        "Morning Dreamers Pancake mix is made from ancient grains, flours and baobab powder to pack even more goodness into your first meal of the day. The mix is gluten-free, wheat-free, dairy-free and can be used to make waffles. Makes 15 pancakes.",
      list_price: "6.55",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: true,
      volume: 936,
      zone: "Ambient",
      created_at: "2016-12-20T11:54:20+00:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2018-12-04T12:30:55+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Superfood-bakery_morning-dreamer-pancakes-1821-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Superfood-bakery_morning-dreamer-pancakes-1821-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0b1ebb90-8b2e-11e8-b8c7-02a4637b1c98",
      sku: "AP-KDN-UTG-30",
      title: "OXO Good Grips Garlic Press Black",
      description:
        "With sturdy, die-cast zinc construction and a large capacity garlic chamber, our Garlic Press crushes garlic with a squeeze. The efficient hole pattern easily pushes garlic through, and a built-in cleaner allows for easy cleaning. Just flip the handles over to clean out excess garlic.",
      list_price: "10.49",
      is_vatable: true,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 1,
      always_on_menu: false,
      volume: 780,
      zone: "Ambient",
      created_at: "2018-07-19T09:30:50+01:00",
      categories: [
        {
          id: "529ea59e-bf7e-11e5-840e-02fada0dd3b9",
          title: "Kitchenware",
          box_limit: 6,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2019-04-09T10:32:14+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Oxo-Good-Grips-Garlic-Press-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Oxo-Good-Grips-Garlic-Press-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0b8f8c86-f1e1-11e6-83f3-027ca04bdf39",
      sku: "AP-ACH-SPL-SM-06-B",
      title: "XXPomegranate Pleaser Cocktail Kit",
      description:
        "You'll fall in love with the refreshing tartness of this pomegranate molasses, gin and lemon juice concoction!\nInside this kit (for 2 cocktails): 1 unique recipe card, 1 unwaxed lemon, 100g pomegranate molasses, 100ml gordon’s gin, 150ml schweppes soda water",
      list_price: "5.95",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: 0,
      zone: "Ambient",
      created_at: "2017-02-13T11:39:14+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-02-13T11:40:32+00:00"
          }
        },
        {
          id: "7867356c-3854-11e6-ab35-06f9522b85fb",
          title: "Mini Alcohol",
          box_limit: 5,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-02-13T11:40:32+00:00"
          }
        },
        {
          id: "287ad10e-e186-11e6-86d4-0297f19e8e45",
          title: "Valentines",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-02-13T11:40:32+00:00"
          }
        },
        {
          id: "a44b4c22-e489-11e6-9d77-027ca04bdf39",
          title: "Most Popular",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-02-13T11:40:32+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-ValentinesCocktail_04293-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-ValentinesCocktail_04293-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0c3603c6-8a66-11e8-b306-02a4637b1c98",
      sku: "G-007",
      title: "Gifting Intro",
      description: "",
      list_price: "0.00",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 1,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2018-07-18T09:39:13+01:00",
      categories: [],
      tags: ["gift"],
      images: {
        "365": null
      }
    },
    {
      id: "0d0537f4-32e8-11e6-968c-06f9522b85fb",
      sku: "AP-ACH-WIN-RED-17-P",
      title: "Terra Vega Reserva, Merlot",
      description:
        "This Chilean Merlot Reserva from the Colchagua Valley is made by Terra Vega, winners of Best Chilean Wine Producer in the International Wine & Spirit Competition in 2012. Its taste has a touch of oak, with delicate herb & cherry aromas, perfect with our Halloumi, Lemon & Courgette Kebabs.\n14% ABV",
      list_price: "7.45",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 1,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-06-15T10:58:11+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-25T10:52:05+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2016-08-25T10:52:05+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Wines-Red-TerraVegaMerlot_19143-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Wines-Red-TerraVegaMerlot_19143-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0d4ac284-63b1-11e6-ae95-023d2759e21d",
      sku: "AP-FCD-CER-06",
      title: "Alara Organic Very Berry Museli",
      description:
        "Muesli inspired by summer: crispy and chewy, very fruity and sweet with wonderful fragrance of abundant strawberries. Whole grains, sunflower seeds and fruits provide healthy balanced nutrition. Very tasty! 600g",
      list_price: "4.15",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 1,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-08-16T12:57:56+01:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-01-30T09:59:10+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-AlaraVeryBerryMuseli_16551-2-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-AlaraVeryBerryMuseli_16551-2-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "0eba7628-8394-11e8-a667-020406e1a8d2",
      sku: "G-103-SR",
      title: "Please ignore",
      description: "Nicola Spring Porcelain Spoon Rest",
      list_price: "0.00",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: null,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2018-07-09T17:20:56+01:00",
      categories: [],
      tags: ["gift"],
      images: {
        "365": null
      }
    },
    {
      id: "102a33a0-e423-11e8-bd56-02dbf738d01a",
      sku: "AP-ACH-BER-17",
      title: "Brewdog Nanny State (Alcohol Free) Pale Ale",
      description:
        "Brewing a full flavoured craft beer at 0.5% is no easy task. Packed with loads of Centennial, Amarillo, Columbus, Cascade and Simcoe hops, dry hopped to the brink. With a backbone of 8 different speciality malts, Nanny State will tantalise your taste buds and leave you wanting more. ABV 0.5%",
      list_price: "1.50",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: true,
      box_limit: 5,
      always_on_menu: true,
      volume: 485,
      zone: "Ambient",
      created_at: "2018-11-09T13:26:28+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2018-11-19T13:12:27+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Brewdog-Nanny-State-1937.2-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Brewdog-Nanny-State-1937.2-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "102cc67e-9bbc-11e8-bb54-02cd9156faf4",
      sku: "AP-FCD-SHB-28",
      title: "BKD Chocolate Dino Mini Baking Kit",
      description:
        "Fuel kids’ imaginations. Make baking with your family easy, tasty & super fun! Chocolate Dino Biscuits Mini Baking Kit comes with everything you will need to create 10-12 of your very own biscuits, they even include a cookie cutter and edible decorations!",
      list_price: "7.95",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: 0,
      zone: "Ambient",
      created_at: "2018-08-09T11:07:46+01:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2018-12-04T12:33:30+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/BKD-Baking-kit_chocolate-dino-biscuits-1816-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/BKD-Baking-kit_chocolate-dino-biscuits-1816-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "10d8c83c-63b2-11e6-95d5-0698ee0b8191",
      sku: "AP-FCD-CSN-23",
      title: "Salt & Black Pepper Cashews",
      description:
        "Deliciously moreish, these cashews are coated in a salt and spicy black pepper seasoning for a real kick of flavour. Don't expect them to last long as a party snack - they'll be gone in seconds!",
      list_price: "1.65",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 4,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-08-16T13:05:11+01:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-31T10:13:30+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Hider_25842-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Hider_25842-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "10e950aa-16b2-11e6-8b39-06de43ff1e17",
      sku: "AP-FCD-CHO-05",
      title: "Green & Blacks Dark 70% Chocolate",
      description:
        "You know the drill: Green & Blacks make classic British chocolate. Three simple ingredients - organic, Fairtrade Trinitario cocoa beans, sugar and vanilla - create a taste that’s simply divine.",
      list_price: "2.10",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 10,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-05-10T13:21:12+01:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-17T13:59:19+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-GBDarkChoc_16581-2-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-GBDarkChoc_16581-2-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "10f2e24c-ead3-11e5-b543-02216daf9ab9",
      sku: "AP-FCD-CSN-10",
      title: "Serious Pig Snackingham Classic ",
      description:
        "It's not a village in Hampshire, it's a snack revelation! Finest British pork is infused with caraway, fennel, coriander and black pepper, cured and air-dried before being sliced into a seriously tasty, bite-sized snack. High in protein and low in fat.",
      list_price: "2.40",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 5,
      always_on_menu: true,
      volume: 176,
      zone: "Ambient",
      created_at: "2016-03-15T17:26:34+00:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-17T13:56:32+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Serious-Pig_Snackingham-Classic-1557_RESIZED-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Serious-Pig_Snackingham-Classic-1557_RESIZED-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "11e06040-aa5d-11e6-82b0-0646ac7a4b17",
      sku: "AP-ACH-BER-04",
      title: "Adnams Dry Hopped Lager",
      description:
        "Bold, bright and tasty, this multi-award winning beer is the first lager from the classic British Adnams Southwold brewery. It's made with pilsner malt and dry hopped with Galaxy hops from Oz, golden & light tropical fruits and hop flavours. \nABV 4.2% \n330ML",
      list_price: "1.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 4,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-11-14T11:25:38+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-11-14T11:28:40+00:00"
          }
        },
        {
          id: "7867356c-3854-11e6-ab35-06f9522b85fb",
          title: "Mini Alcohol",
          box_limit: 5,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2016-11-15T14:23:11+00:00"
          }
        },
        {
          id: "a44b4c22-e489-11e6-9d77-027ca04bdf39",
          title: "Most Popular",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-04-28T13:28:00+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Beers_02003-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Beers_02003-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1428fc7c-f757-11e5-a91d-02787aad01f3",
      sku: "AP-KDN-UTG-11",
      title: "Joseph Joseph Duo-Blade",
      description:
        "These spring-hinged scissors double are safe, intuitive and fold away tidily for easy storage. They also double up as box cutters, perfect for getting into your Gousto box and snipping open any packaged ingredients inside!",
      list_price: "9.95",
      is_vatable: true,
      is_for_sale: true,
      age_restricted: true,
      box_limit: 5,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-03-31T15:41:47+01:00",
      categories: [
        {
          id: "529ea59e-bf7e-11e5-840e-02fada0dd3b9",
          title: "Kitchenware",
          box_limit: 6,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-22T13:44:22+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-JJ-TwinCut_16594-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-JJ-TwinCut_16594-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "16f456f0-577f-11e9-83bc-06e2ed19d23a",
      sku: "AP-FCD-CSN-38",
      title: "Trio of Chocolate Popcorn Joe & Sephs (80g) ",
      description:
        "For popcorn and chocolate lovers alike, Joe & Seph's have combined their signature gourmet popcorn with a tasty mix of White Chocolate, Milk Chocolate and Cookies & Cream. This popcorn is air-popped rather than fried.",
      list_price: "2.99",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: false,
      volume: 1200,
      zone: "Ambient",
      created_at: "2019-04-05T09:44:56+01:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2019-04-09T09:35:42+01:00"
          }
        },
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2019-04-09T09:35:42+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Trio-of-choc-Joe--Sephs-Popcorn-2-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Trio-of-choc-Joe--Sephs-Popcorn-2-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "184154e8-4589-11e9-9b34-02492a0dc9b6",
      sku: "G-139",
      title: "Deep Roast Creamy Peanut Butter - Gift",
      description: "",
      list_price: "0.00",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 1,
      always_on_menu: false,
      volume: 504,
      zone: "Ambient",
      created_at: "2019-03-13T12:11:13+00:00",
      categories: [],
      tags: ["gift"],
      images: {
        "365": null
      }
    },
    {
      id: "18b0d6ec-26d1-11e8-8649-0239a66a4b36",
      sku: "AP-ACH-WIN-WHI-32-P",
      title: "La Lisse Soie d'Ivoire Chenin Blanc",
      description:
        "This wine is fantastically food friendly. La Lisse Soie d'Ivoire translates as 'soft ivory silk', which gives a clue to the attractive texture of this sensual Chenin Blanc. Hints of caramel and brioche lead lead to a densely fruity peach palate. A clean, lifted finish. 12.5% ABV [France]",
      list_price: "9.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: 0,
      zone: "Ambient",
      created_at: "2018-03-13T15:13:33+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2018-03-13T15:14:16+00:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2018-03-13T15:14:16+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/La-Lisse-Soie-Chenin-Blanc_Market-Place0588-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/La-Lisse-Soie-Chenin-Blanc_Market-Place0588-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "18ebd622-33eb-11e7-bfc8-060907791b25",
      sku: "AP-ACH-WIN-WHI-27-P",
      title: "The Cloud Factory Sauvignon Blanc, Marlborough",
      description:
        "New Zealand's Marlborough region has created this classically bright and vibrant Sauvignon Blanc. Flavours of guava, lime and tropical fruits combine to create a modern, clean white that best complements shellfish and vegetarian dishes.",
      list_price: "9.95",
      is_vatable: true,
      is_for_sale: true,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-05-08T13:37:28+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-05-08T13:37:47+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-05-08T13:37:47+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/YAddOns-WhiteWines-CloudFactory_013242-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/YAddOns-WhiteWines-CloudFactory_013242-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1907b434-f71d-11e5-887e-02787aad01f3",
      sku: "AP-ACH-WIN-WHI-06-P",
      title: "Camino Real Blanco Rioja",
      description:
        "This white Rioja goes particularly well with mildly spiced dishes, and will be great with chicken and fish recipes such as our Light Citrus Chicken. Notes of date and banana make it a flavoursome, interesting wine with just the right amount of complexity. ABV 12.5%",
      list_price: "7.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-03-31T08:46:45+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-25T10:52:19+01:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2016-08-25T10:52:19+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Wines-CaminoReal_05730-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Wines-CaminoReal_05730-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "19602064-3d1e-11e6-9272-06f9522b85fb",
      sku: "AP-ACH-SPL-SM-02-P",
      title: "Zingiberry Cocktail Kit",
      description:
        "Named after 'zingiber' (ginger's Latin name), redcurrant jelly, lime and vodka are mixed with ginger ale in this fun, fancy zinger.\nInside the kit (for 2 cocktails): 1 unique recipe card, 1 lime, 30g dried cranberries, 50g rich redcurrant jelly, 100ml smirnoff vodka, 150ml schweppes ginger ale",
      list_price: "6.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-06-28T10:50:16+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-16T11:58:53+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Summer-Cocktail-Marketplace-Images_zingiberry-01-2-x400.png",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Summer-Cocktail-Marketplace-Images_zingiberry-01-2-x400.png",
          width: 400
        }
      }
    },
    {
      id: "1981d957-9b58-4962-bf65-28efe008ea6f",
      sku: "G-058",
      title: "Festive GT flyer",
      description: "Festive GT flyer",
      list_price: "0.00",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: null,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-11-08T14:00:08+00:00",
      categories: [],
      tags: ["gift"],
      images: {
        "365": null
      }
    },
    {
      id: "1b7a94ce-340d-11e7-9424-02859a19531d",
      sku: "AP-ACH-BER-06",
      title: "Belleville Brewing Co. Picnic Pale",
      description:
        "Eight different hops combine to give this easy-drinking IPA its citrusy-piney blast of aroma and palate-tingling bitterness. True to its name, this is a perfect picnic beer (but we reckon it's just as nice when enjoyed at home).",
      list_price: "2.20",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 4,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-05-08T17:40:55+01:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-05-08T17:41:23+01:00"
          }
        },
        {
          id: "7867356c-3854-11e6-ab35-06f9522b85fb",
          title: "Mini Alcohol",
          box_limit: 5,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-05-08T17:41:23+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Beers-PicnicSessionIPA_013281-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Beers-PicnicSessionIPA_013281-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1c66afae-e414-11e8-b340-06bb8c522d92",
      sku: "AP-FCD-CON-19",
      title: "Manilife Original Peanut Butter (295g)",
      description:
        "An award winning lightly roasted single estate peanut butter with the perfect combination of creaminess and crunch. These beauties are naturally sweeter than your average nut and with fat profile more akin to olive oil, better for you too. Allergen: Peanut",
      list_price: "3.99",
      is_vatable: false,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 4,
      always_on_menu: true,
      volume: 450,
      zone: "Ambient",
      created_at: "2018-11-09T11:39:26+00:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2018-11-20T18:16:31+00:00"
          }
        },
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2019-03-27T16:06:24+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Manilife-original-1863.2-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Manilife-original-1863.2-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1d05ea96-de72-11e6-ac5e-02272d35a40b",
      sku: "AP-FCD-CHO-17",
      title: "Mighty Fine Honeycomb Salted Caramel Dips",
      description:
        "It doesn’t get more irresistible than this signature light & crisp honeycomb. Flavoured with sea salt & wrapped in caramel-infused Belgian milk chocolate – it’s a treat which makes a real difference – with money from each pack sold going to the Friends of the Honey Bee campaign. ",
      list_price: "3.55",
      is_vatable: true,
      is_for_sale: true,
      age_restricted: false,
      box_limit: 2,
      always_on_menu: true,
      volume: 1200,
      zone: "Ambient",
      created_at: "2017-01-19T18:07:17+00:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-01-19T18:07:55+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Honeycomb-Salted-Caramel-Dips_Market-Place-2318_RESIZED-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/Honeycomb-Salted-Caramel-Dips_Market-Place-2318_RESIZED-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1d8d2ccc-098d-11e7-b533-02d38929c515",
      sku: "AP-FCD-CHO-31",
      title: "Slabb Bar- The Millionaires One",
      description:
        "Filled with smooth caramel and crunchy biscuit pieces, the Millionaire's bar is full of luxury goodness. The 70% Belgian chocolate bar drizzled with quality white chocolate is the perfect finish to the perfect Easter.",
      list_price: "4.45",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 4,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-03-15T14:38:54+00:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-15T14:42:14+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image/AddOns-Slabb-TheMillionairesOne_05994-1-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image/AddOns-Slabb-TheMillionairesOne_05994-1-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1da65e86-5af1-11e6-9ce2-0698ee0b8191",
      sku: "AP-FCD-CER-05",
      title: "Biona Organic Choco Coconut Granola",
      description:
        "A scrumptious blend of wholegrain oats, dreamy chocolate and coconut flakes that create a perfectly balanced, totally delicious breakfast bowl that's as healthy as it is tasty. 375g",
      list_price: "3.55",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 1,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-08-05T09:43:51+01:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-01-30T09:59:32+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-BionaChocoCocoGranola_16563-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-BionaChocoCocoGranola_16563-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1ddd3dcc-025b-11e7-893c-0234acc3da5b",
      sku: "AP-FCD-CHO-22",
      title: "Choc on Choc White and Dark Belgian Chocolate Quail Eggs",
      description:
        "Decorated with a beautiful and eye-catching butterfly design, these solid dark and white chocolates are the perfect treat. Presented in a traditional egg box, these choc on choc quail eggs are as impressive as they are tasty!",
      list_price: "4.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 4,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-03-06T10:53:22+00:00",
      categories: [
        {
          id: "17eb3f8e-bf7e-11e5-ab63-02fada0dd3b9",
          title: "Snacks",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-06T12:06:26+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-ChocOnChoc-QuailEggs_05978-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-ChocOnChoc-QuailEggs_05978-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1e8b41f0-025c-11e7-9a3b-02d559812e87",
      sku: "AP-ACH-WIN-RED-23-P",
      title: "Marques de Calado Tempranillo Garnacha",
      description:
        "Combining ripe, dark cherry with the soft, sweet flavour of raspberry - this medium-bodied wine is complemented by cinnamon and clove spice for the perfect finish. ABV 12.5%",
      list_price: "6.95",
      is_vatable: true,
      is_for_sale: false,
      age_restricted: true,
      box_limit: 2,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2017-03-06T11:00:33+00:00",
      categories: [
        {
          id: "faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9",
          title: "Drinks Cabinet",
          box_limit: 7,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2017-03-06T11:43:15+00:00"
          }
        },
        {
          id: "785741fc-3854-11e6-87a5-06f9522b85fb",
          title: "Large Alcohol",
          box_limit: 2,
          is_default: false,
          recently_added: false,
          hidden: true,
          pivot: {
            created_at: "2017-03-06T11:43:15+00:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Wines-Castilla_05723-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-Wines-Castilla_05723-x400.jpg",
          width: 400
        }
      }
    },
    {
      id: "1f3e47c2-1780-11e6-801a-06de43ff1e17",
      sku: "AP-FCD-SHB-08",
      title: "Traidcraft Golden Caster Sugar",
      description:
        "This quality unrefined sugar doesn’t just taste good, it does good. Grown, processed and packed in Mauritius, it helps to support an island economy and is also Fairtrade certified. Sweet!",
      list_price: "1.40",
      is_vatable: false,
      is_for_sale: false,
      age_restricted: false,
      box_limit: 1,
      always_on_menu: false,
      volume: null,
      zone: null,
      created_at: "2016-05-11T13:56:12+01:00",
      categories: [
        {
          id: "01b06fa0-bf7e-11e5-9c1e-02fada0dd3b9",
          title: "Food Cupboard",
          box_limit: 10,
          is_default: false,
          recently_added: false,
          hidden: false,
          pivot: {
            created_at: "2016-08-30T09:23:38+01:00"
          }
        }
      ],
      tags: [],
      images: {
        "365": {
          src:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-GoldenCasterSugar_16940-x400.jpg",
          url:
            "https://production-media.gousto.co.uk/cms/product-image-landscape/AddOns-GoldenCasterSugar_16940-x400.jpg",
          width: 400
        }
      }
    }
  ]
};
