export const FONT_COLOR = "#41474e";
export const WHITE = "#ffffff";
export const BLUE = "#60aae4";
export const DARK_GREY = "#585858";
export const GREY = "#CECECE";
export const RED = "#f6323e";
