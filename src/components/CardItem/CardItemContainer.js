import React, { Component } from "react";
import PropTypes from "prop-types";
import CardItem from "./CardItem";

class CardItemContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      description: false
    };
  }

  onClick = () => {
    const { description } = this.state;

    this.setState({
      description: !description
    });
  };

  render() {
    const { product } = this.props;
    const { description } = this.state;

    return (
      <CardItem
        product={product}
        description={description}
        onClick={this.onClick}
      />
    );
  }
}

CardItemContainer.propTypes = {
  product: PropTypes.shape({}).isRequired
};

export default CardItemContainer;
