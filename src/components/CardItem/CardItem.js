import React from "react";
import PropTypes from "prop-types";
import { Card, Image } from "semantic-ui-react";
import { styles } from "./styles";

const CardItem = ({ product, description, onClick }) => (
  <Card style={styles.card} data-cy="product-item" onClick={onClick}>
    {product && (
      <Image
        src={product.images["365"] ? product.images["365"].src : ""}
        wrapped
        ui={false}
      />
    )}
    <Card.Content>
      <Card.Header>{product.title}</Card.Header>
      <Card.Meta>£{product.list_price}</Card.Meta>
      {description && (
        <Card.Description data-cy="product-item-description">
          {product.description}
        </Card.Description>
      )}
    </Card.Content>
  </Card>
);

CardItem.propTypes = {
  product: PropTypes.shape({}).isRequired,
  description: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default CardItem;
