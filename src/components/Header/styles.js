import { WHITE, RED } from "../../styles";

export const styles = {
  header: {
    position: "fixed",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1,
    left: 0,
    top: 0,
    backgroundColor: RED
  },
  headerContainer: {
    width: "100%",
    maxWidth: "1300px",
    display: "flex",
    height: 60,
    justifyContent: "space-between",
    alignItems: "center"
  },
  logo: {
    width: 54,
    marginLeft: 10
  },
  menu: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 39
  },
  sidebar: {
    position: "fixed",
    height: "calc(100vh - 120px)",
    width: 250,
    right: 0,
    top: 120,
    display: "flex",
    flexDirection: "column",
    backgroundColor: RED
  },
  buttonBarsWhite: {
    fontSize: 20,
    cursor: "pointer",
    width: 50,
    textAlign: "center",
    color: WHITE
  },
  hidden: {
    display: "none"
  }
};
