import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Header from "./Header";

class HeaderContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSidebarVisible: false
    };
  }

  onClick = () => {
    const { isSidebarVisible } = this.state;
    this.setState({
      isSidebarVisible: !isSidebarVisible
    });
  };

  render() {
    const { categories } = this.props;
    const { isSidebarVisible } = this.state;

    return (
      <Header
        categories={categories}
        isSidebarVisible={isSidebarVisible}
        onClick={this.onClick}
      />
    );
  }
}

HeaderContainer.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

const mapStateToProps = state => ({
  categories: state.categories.categories
});

export default connect(
  mapStateToProps,
  null
)(HeaderContainer);
