import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Responsive from "react-responsive";
import { Link, withRouter } from "react-router-dom";
import { Icon } from "semantic-ui-react";
import uuidv4 from "uuid/v4";
import fast from "fast.js";
import CategoryItem from "../CategoryItem";
import logo from "../../assets/favicon.ico";
import { HOMEPAGE } from "../../config/routes";
import { styles } from "./styles";

const Header = ({ categories, isSidebarVisible, onClick }) => (
  <Fragment>
    <header style={styles.header} data-cy="header">
      <div style={styles.headerContainer}>
        <Link to={HOMEPAGE}>
          <img src={logo} alt="logo" data-cy="logo" style={styles.logo} />
        </Link>
        <div style={styles.menu}>
          <Responsive minWidth={1024}>
            {categories &&
              fast.map(categories, category => {
                return (
                  <CategoryItem
                    title={category.title}
                    key={"category-" + uuidv4()}
                  />
                );
              })}
          </Responsive>
          <Responsive maxWidth={1023}>
            <Icon
              style={styles.buttonBarsWhite}
              data-cy="header-menubutton"
              className="bars"
              onClick={onClick}
            />
            {isSidebarVisible && (
              <div style={styles.sidebar}>
                {categories &&
                  fast.map(categories, category => {
                    return (
                      <CategoryItem
                        title={category.title}
                        key={"category-" + uuidv4()}
                      />
                    );
                  })}
              </div>
            )}
          </Responsive>
        </div>
      </div>
    </header>
  </Fragment>
);

Header.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isSidebarVisible: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default withRouter(Header);
