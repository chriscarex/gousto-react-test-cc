import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import SearchBar from "./SearchBar";
import { setSearch } from "../../redux/actions";

class SearchBarContainer extends Component {
  onChange = e => {
    const { setSearchAction } = this.props;
    setSearchAction(e.target.value);
  };

  render() {
    const { search } = this.props;

    return <SearchBar value={search} onChange={this.onChange} />;
  }
}

SearchBarContainer.propTypes = {
  search: PropTypes.string.isRequired,
  setSearchAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  search: state.search.selectedCategory
});

const mapDispatchToProps = dispatch => {
  return {
    setSearchAction: props => dispatch(setSearch(props))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBarContainer);
