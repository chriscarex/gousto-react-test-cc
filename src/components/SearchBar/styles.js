import { GREY } from "../../styles";

export const styles = {
  container: {
    position: "fixed",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1,
    left: 0,
    top: 60,
    backgroundColor: GREY
  },
  internalContainer: {
    width: "100%",
    maxWidth: "1300px",
    display: "flex",
    height: 60,
    justifyContent: "space-between",
    alignItems: "center"
  },
  inputContainer: {
    position: "relative"
  },
  input: {
    marginLeft: 20,
    width: 280
  },
  search: {
    position: "absolute",
    left: 280,
    top: 10
  }
};
