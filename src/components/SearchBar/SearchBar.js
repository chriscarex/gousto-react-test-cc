import React from "react";
import PropTypes from "prop-types";
import { Input, Icon } from "semantic-ui-react";

import { styles } from "./styles";

const SearchBar = ({ value, onChange }) => (
  <div style={styles.container}>
    <div style={styles.internalContainer}>
      <div style={styles.inputContainer}>
        <Input
          style={styles.input}
          placeholder="Search"
          data-cy="search"
          value={value}
          onChange={onChange}
        />
        <Icon name="search" style={styles.search} />
      </div>
    </div>
  </div>
);

SearchBar.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default SearchBar;
