import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCategories, getProducts } from "../../redux/actions";

class Startup extends Component {
  componentWillMount() {
    const { getCategoriesAction, getProductsAction } = this.props;

    getCategoriesAction();
    getProductsAction();
  }

  render() {
    return <Fragment />;
  }
}

Startup.propTypes = {
  getCategoriesAction: PropTypes.func.isRequired,
  getProductsAction: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => {
  return {
    getCategoriesAction: props => dispatch(getCategories(props)),
    getProductsAction: props => dispatch(getProducts(props))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Startup);
