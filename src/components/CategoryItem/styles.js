import { WHITE } from "../../styles";

export const styles = {
  menuItemBold: {
    fontSize: 20,
    color: WHITE,
    marginLeft: 10,
    marginRight: 10,
    fontWeight: 700,
    cursor: "pointer",
    backgroundColor: "transparent"
  },
  menuItem: {
    fontSize: 10.5,
    color: WHITE,
    marginLeft: 10,
    marginRight: 10,
    cursor: "pointer",
    backgroundColor: "transparent"
  },
  menuItemMobileBold: {
    width: "100%",
    textAlign: "center",
    fontSize: 20,
    color: WHITE,
    marginLeft: 10,
    marginRight: 10,
    fontWeight: 700,
    cursor: "pointer",
    backgroundColor: "transparent",
    padding: 20
  },
  menuItemMobile: {
    width: "100%",
    textAlign: "center",
    fontSize: 14,
    padding: 20,
    color: WHITE,
    marginLeft: 10,
    marginRight: 10,
    cursor: "pointer",
    backgroundColor: "transparent"
  }
};
