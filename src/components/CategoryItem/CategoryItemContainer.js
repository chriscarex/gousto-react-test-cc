import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import CategoryItem from "./CategoryItem";
import { setCategory } from "../../redux/actions";

class CategoryItemContainer extends Component {
  render() {
    const { title, category } = this.props;

    console.log(title, category);
    return <CategoryItem title={title} selectedCategory={category} />;
  }
}

CategoryItemContainer.propTypes = {
  title: PropTypes.string.isRequired,
  setCategoryAction: PropTypes.func.isRequired,
  category: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  category: state.categories.selectedCategory
});

const mapDispatchToProps = dispatch => {
  return {
    setCategoryAction: props => dispatch(setCategory(props))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryItemContainer);
