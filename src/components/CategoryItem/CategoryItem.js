import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Responsive from "react-responsive";
import { Link, withRouter } from "react-router-dom";
import { titleToUrl } from "../../utils";
import { styles } from "./styles";

const CategoryItem = ({ title, onClick, selectedCategory }) => (
  <Fragment>
    <Responsive minWidth={1024}>
      <Link
        to={`/category/` + titleToUrl(title)}
        style={
          selectedCategory === title ? styles.menuItemBold : styles.menuItem
        }
        data-cy="category-item"
      >
        {title}
      </Link>
    </Responsive>
    <Responsive maxWidth={1023}>
      <Link
        to={`/category/` + titleToUrl(title)}
        style={
          selectedCategory === title
            ? styles.menuItemMobileBold
            : styles.menuItemMobile
        }
        data-cy="category-item"
      >
        {title}
      </Link>
    </Responsive>
  </Fragment>
);

CategoryItem.propTypes = {
  title: PropTypes.string.isRequired,
  selectedCategory: PropTypes.string.isRequired
};

export default withRouter(CategoryItem);
